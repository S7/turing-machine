Turing Machine
=====================

Usage
-----

Compile: javac TuringMachine.java


Run: java TuringMachine <input file>



Format of input file
--------------------

First line of the file is the tape, e.g. `*111010101*` (`*` denotes a blank cell).
The rest of the file is a table with 5 columns on the form `state read write direction newState`. 

- `state` is the current state that the turing machine is in. The initial state is called `start`.
- `read` is the symbol that the turing machine reads using its read/write-head. At the very beginning, the r/w-head is positioned on the `*` just to the right of the initial data.
- `write` is the symbol to write in that cell.
- `direction` is the direction that the r/w-head should move, denoted by `<-` (left), `->` (right) and any other symbol will not move the head.
- `newState` is the new state that the turing machine will be in.

Special states involve: 

- `start` - the start state that the simulator will start in
- `halt` - the state that will halt the machine

The symbols allowed to be read and to be written is denoted as the alphabet of the turing machine. Currently, the alphabet for the simulator is `* 0 1 2 3 4 5 6 7 8 9`.

Note: The simulator will automatically insert `*` if the r/w-head reaches the egde of the tape (as it actually is infinitely long in both directions). 

Example
-------

    10110101
    start * * <- shift
    shift 0 0 <- shift0
    shift 1 0 <- shift1
    shift * * - halt
    shift0 0 0 <- shift0
    shift0 1 0 <- shift1
    shift0 * 0 - halt
    shift1 0 1 <- shift0
    shift1 1 1 <- shift1
    shift1 * 1 - halt
    
The turing machine above will shift every bit to the left (equivalent to multiply by 2).


Debugging
---------

The simulator supports two levels of debugging: Tape output for every step and full step by step walkthrough including tape output. 
In the source code, if the variable `debug` is true, the simulator will give a full step by step walkthrough. If it is false, and `tapeDebug` is true, it will only give tape output for every step. 

The simulator has a step limit of 5000 steps. 
