import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author Peter Severin Rasmussen
 */
public class TuringMachine {

    private static String empty = "*";
    private static String[] alphabet = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static String startState = "start";
    private static String[] acceptingStates = {"halt"};
    private static String haltState = "halt";
    private static int headPos;
    private static String delimiter = " ";
    private static String map_delimiter = ",";
    private static Map<String, String> source = new HashMap<String, String>();
    private static int steps = 0;
    private static int stepLimit = 5000;
    private static boolean startLeft = false;
    private static String tape = "";

    private static boolean debug = true;
    private static boolean tapeDebug = true;

    public static void main(String[] args) {
        if (args.length > 0) {
            run(args[0]);
        }
    }

    private static Scanner load(String filename) {
        File file = new File(filename);
        Scanner sc;

        try {
            sc = new Scanner(file);
            return sc;
        } catch (FileNotFoundException e) {
            System.out.println("'" + filename + "' not found.");
            System.out.println("Java Exception: " + e);
        }

        return null;
    }

    public static void run(String filename) {
        Scanner sc = load(filename);
        String l;
        if (sc == null) {
            return;
        }

        String[] split;
        String key;
        String value;
        boolean firstLine = true;

        while (sc.hasNext()) {
            l = sc.nextLine().trim();
            if (!l.equals("")) {
                if (firstLine) {
                    tape = l;
                    firstLine = false;
                    continue;
                }
                split = l.split(delimiter);
                if (split.length != 5) {
                    System.out.println("Invalid line: '" + l + "' - Must have exactly 5 columns");
                    continue;
                }
                key = split[0] + map_delimiter + split[1];
                value = split[2] + map_delimiter + split[3] + map_delimiter + split[4];
                source.put(key, value);
            }
        }

        if (startLeft) {
            int firstEmpty = tape.indexOf(empty);
            if (firstEmpty > 0) {
                headPos = firstEmpty;
            } else {
                tape = empty + tape;
                headPos = 0;
            }
        } else {
            int lastEmpty = tape.lastIndexOf(empty);
            if (lastEmpty > 0) {
                headPos = lastEmpty;
            } else {
                tape = tape + empty;
                headPos = tape.length() - 1;
            }
        }

        drawTape();
        execute(startState + map_delimiter + empty);
        drawTape();
    }

    public static void execute(String key) {

        String[] k_split = key.split(map_delimiter);
        String state = k_split[0];
        String read = k_split[1];

        String value = source.get(key);

        if (value == null) {
            System.out.println("No table entry for " + state + " " + read);
            return;
        }

        String[] v_split = value.split(map_delimiter);

        String write = v_split[0];
        String direction = v_split[1];
        String newState = v_split[2];

        if (headPos < 0) {
            tape = empty + tape;
            headPos++; // Shift the head
        } else if (headPos >= tape.length()) {
            tape = tape + empty;
        }
        
        if (debug) {
            System.out.println("");
            System.out.println("Step " + (steps + 1) + ":");
            drawTape();
            System.out.println("In state '" + state + "', read '" + read + "'");
        } else if (tapeDebug) {
            drawTape();
        }
        
        if (!inAlphabet(write)) {
            System.out.println(write + " not in alphabet.");
            return;
        }

        StringBuilder newTape = new StringBuilder(tape);
        newTape.setCharAt(headPos, write.charAt(0));
        tape = newTape.toString();
        if (debug) {
            System.out.println("Write '" + write + "'");
        }

        if (direction.equals("<-")) {
            headPos--;
            if (debug) {
                System.out.println("Move the head to the left (<-)");
            }
        } else if (direction.equals("->")) {
            headPos++;
            if (debug) {
                System.out.println("Move the head to the right (->)");
            }
        }

        String newRead;
        if (0 <= headPos && headPos < tape.length()) {
            newRead = Character.toString(tape.charAt(headPos));
        } else {
            newRead = empty;
        }

        steps++;

        if (steps > stepLimit) {
            System.out.println("Turing Machine reached step limit.");
            return;
        }

        if (debug) {
            System.out.println("Go to state '" + newState + "'");
            System.out.println("");
        }

        if (!newState.equals(haltState)) {
            execute(newState + map_delimiter + newRead);
        } else {
            System.out.println("Turing Machine has halted.");
        }
    }

    private static void drawTape() {
        String prefix = "Tape: ";
        System.out.println(prefix + tape);
        StringBuilder padding_sb = new StringBuilder();
        for (int i = -prefix.length(); i < headPos; i++) {
            padding_sb.append(" ");
        }
        String padding = padding_sb.toString();
        System.out.println(padding + "^");
    }

    private static boolean inAlphabet(String s) {
        if (s.equals(empty)) {
            return true;
        }
        for (String symbol : alphabet) {
            if (s.equals(symbol)) {
                return true;
            }
        }
        return false;
    }
}
